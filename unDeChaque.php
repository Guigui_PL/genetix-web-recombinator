<?php

	define('Framework', true);
	//$cache = false;
	require_once('includes/init.php');
	$titre = t('Liste des fonctions');
	$tpl = new Smarty;
	
	require_once('includes/entete.php');
	
	$logicManager = new LogicManager($bdd);
	$wordsManager = new WordsManager($bdd);
	
	$pagination = new Pagination(50, $logicManager->getNombre(), 'unDeChaque.php');
	if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
	$pagination->setPremier(true);
	$logics = $logicManager->getListe($pagination, null, array(['champ' => 'nb_inputs', 'sens' => DB::ORDRE_ASC], ['champ' => 'output', 'sens' => DB::ORDRE_ASC]));
	
	$liste = [];
	
	foreach ($logics as $logic)
	{
	    $veritas = new VeritasLogic($logic);
	    $liste[] = $wordsManager->getWord(array('output', $veritas->outputToString()), array(['champ' => 'nb_genes', 'sens' => DB::ORDRE_ASC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
	}
	
	$tpl->assign(array(
		'listeSequences' => $liste,
		'pages' => $pagination->getPages()));
		
	$tpl->display('listSeq.html');
	require_once('includes/piedDePage.php');