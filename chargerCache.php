<?php

	define('Framework', true);
	$titre = 'Accueil';
	$cache = false;
	require_once('includes/init.php');
	
	$wordsManager = new WordsManager($bdd);
	$logicManager = new LogicManager($bdd);
	
	$req = $bdd->query("SELECT id_lf FROM logical_functions");
	
	while ($result = $req->fetch(PDO::FETCH_ASSOC))
	{
	    $wordsManager->getNombre($result['id_lf']);
	}
	
	
	$pagination = new Pagination(3000000, $logicManager->getNombre(), '');
	$orderBy = array(['champ' => 'nb_inputs', 'sens' => DB::ORDRE_ASC], ['champ' => 'ndf', 'sens' => DB::ORDRE_ASC]);
	$listeFonctions = $logicManager->getListe($pagination, null, $orderBy);
	
	foreach ($listeFonctions as $fonction)
	{
	    $veritas = new VeritasLogic($fonction);
	    $output = $veritas->outputToString();
	    $stop = min(ceil($wordsManager->getNombre($fonction->getId_fn()))/30, 10);
	    $pagination = new Pagination(30, $wordsManager->getNombre($fonction->getId_fn()), '');
	    
	    $wordsManager->getListe($pagination, array('ndf', bindec($_GET['output']), DB::SQL_AND, 'nb_inputs', $_GET['nbInputs']), array(['champ' => 'weak_constraint', 'sens' => DB::ORDRE_DESC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
	}
	
	foreach ($listeFonctions as $fonction)
	{
	    $veritas = new VeritasLogic($fonction);
	    $output = $veritas->outputToString();
	    $stop = min(ceil($wordsManager->getNombre($fonction->getId_fn()))/30, 10);
	    $pagination = new Pagination(30, $wordsManager->getNombre($fonction->getId_fn()), '');
	    
	    for ($i = 1; $i <= $stop; ++$i)
	    {
		$pagination->setPageActuelle($i);
		$wordsManager->getListe($pagination, array('ndf', bindec($_GET['output']), DB::SQL_AND, 'nb_inputs', $_GET['nbInputs']), array(['champ' => 'weak_constraint', 'sens' => DB::ORDRE_DESC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
	    }
	}