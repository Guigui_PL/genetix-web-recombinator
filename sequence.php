<?php

	define('Framework', true);
	$titre = 'Séquence';
	//$cache = false;
	require_once('includes/init.php');
	$tpl = new Smarty;
	require_once('includes/entete.php');
	
	$wordsManager = new WordsManager($bdd);
	
	if (empty($_GET['names']))
            $word = $wordsManager->getWord(array('s.id_s', $_GET['id_w']));
        else
            $word = $wordsManager->getWord(array('s.id_s', $_GET['id_w'], DB::SQL_AND, 'names', $_GET['names']));
	
	$tpl->assign(array(
		'word' => $word,
		'veritas' => new VeritasWord($word)));
	
	$tpl->display('sequence.html');
	require_once('includes/piedDePage.php');
