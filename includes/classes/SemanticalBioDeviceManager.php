<?php
#################################################
#						#
#	SemanticalBioDeviceManager.php			#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

    if ( !defined('Framework') ) exit;  
    
    class SemanticalBioDeviceManager
    {
	use ToolsForManagers;
	    
	public function __construct ($bdd)
	{
	    $this->setBdd($bdd);
	}
	    
	public function getListe (Pagination $pagination, $listeParametres = null, $ordre = null)
	{
	    if ($listeParametres != null || $ordre != null)
		$champs = $this->listeColonnes(['semantical_bio_device', 'semantics', 'dyck_functionnal_structure', 'implements', 'namings', 'dyck_words']);
	    else $champs = null;
		    
	    $requete = "SELECT *
			    FROM semantical_bio_device sbd
			    JOIN semantics s ON sbd.id_semantics=s.id_semantics
			    JOIN dyck_functionnal_structure dfs ON sbd.id_dyck_functionnal_structure=dfs.id_dyck_functionnal_structure
			    ";
	    
	    $nomCache = md5($requete.serialize($champs).serialize($listeParametres).serialize($ordre).serialize($pagination->getLimit()));
	    
	    $$nomCache = new CacheArray($nomCache, 0); 
	    $cacheArrayManager = new CacheArrayManager;
	    $cacheArrayManager->readCache($$nomCache);
	    
	    if ($cacheArrayManager->readCache($$nomCache) !== false)  return $$nomCache;
	    else
	    {
		$req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, $pagination->getLimit());
		$liste = $this->genereListe($req, 'SemanticalBioDevice');
		
		$$nomCache->setContenu($liste); 
		$cacheArrayManager->writeCache($$nomCache);
		
		return $liste;
	    }
	}
	    
	public function getWord ($listeParametres = null, $ordre = null)
	{
	    if ($listeParametres != null || $ordre != null)
		$champs = $this->listeColonnes(['sequences', 'logical_functions', 'sequences_features', 'implements', 'namings', 'dyck_words']);
	    else $champs = null;
	    
	    $champs[] = "s.id_s";
		    
	    $requete = "SELECT s.id_s, sequence, weak_constraint, strong_constraint, length, word, names
			    FROM sequences s
			    JOIN implements i ON i.id_s=s.id_s
			    JOIN logical_functions lf ON lf.permutation_class=i.permutation_class 
			    JOIN sequences_features sf ON sf.id_sf=s.id_sf  
			    JOIN dyck_words dw ON dw.id_dw=s.id_dw
			    JOIN namings n ON n.id_n=i.id_n
			    ";
	    
	    $nomCache = md5($requete.serialize($champs).serialize($listeParametres).serialize($ordre).serialize(" LIMIT 1"));
	    
	    $$nomCache = new CacheArray($nomCache, 0); 
	    $cacheArrayManager = new CacheArrayManager;
	    $cacheArrayManager->readCache($$nomCache);
	    
	    if ($cacheArrayManager->readCache($$nomCache) !== false)  return $$nomCache->getContenu();
	    else
	    {
		$req = $this->executeRequeteListe($requete, $champs, $listeParametres, $ordre, " LIMIT 1");
		$liste = $this->genereListe($req, 'Word');
		
		$$nomCache->setContenu($liste[0]); 
		$cacheArrayManager->writeCache($$nomCache);
		
		return $liste[0];
	    }
	}
	
	public function getNombre ($permutation_class = null)
	{
	    if ($permutation_class != null && is_numeric($permutation_class)) $reqPermutation_class = " WHERE permutation_class = :permutation_class ";
	    else $reqPermutation_class = '';
	    
	    $req = $this->_bdd->prepare("SELECT COUNT(*) AS count FROM semantical_bio_device ".$reqPermutation_class);
	    
	    if ($permutation_class != null && is_numeric($permutation_class)) $req->bindValue(':permutation_class', $permutation_class, PDO::PARAM_INT);
	    $cache = $req->executeWithCache(null, 0, 'nb_semanticalBioDevice_'.$permutation_class);
	    
	    return $cache->fetch(PDO::FETCH_ASSOC)['count'];
	}
    }
