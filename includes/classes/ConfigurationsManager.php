<?php
#################################################
#						#
#	ConfigurationsManager.php		#
#	class pour gérer les objets 		#
#	Configuration				#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;  
	
	class ConfigurationsManager
	{
		private $_listeFichiers;
		
		public function __construct ()
		{
			$this->setListeFichiers();
		}
		
		/**
		 * Méthode setListeFichiers ()
		 * Permet de charger la liste des fichiers de configuration et leurs réglages dans un objet de classe Configuration 
		 * @return void
		 */
		
		public function setListeFichiers ()
		{
			require ($GLOBALS['cheminRacine']."config/listeFichiers.php");
			
			$this->_listeFichiers = new Configuration(Configuration::TYPE_TABLEAU_PHP);
			$this->_listeFichiers->setVariables(["listeFichiers"]);
			$this->_listeFichiers->setContenu(["listeFichiers" => $listeFichiers]);
		}
		
		/**
		 * Méthode getConfiguration ()
		 * Permet de charger un fichier de configuration dans un objet Configuration
		 * @param string $fichier
		 * @return mixed
		 */
		 
		public function getConfiguration ($fichier)
		{
			$confFichier = $this->_listeFichiers->getContenu()["listeFichiers"][$fichier];
			$type = $confFichier["type"];
			$configuration = new Configuration($type);
			$configuration->setNomFichier($fichier);
			
			if ($type == Configuration::TYPE_TEXTE)
			{
				$configuration->setContenu(file_get_contents($GLOBALS['cheminRacine']."config/".$fichier));
				return $configuration;
			}
			else if ($type == Configuration::TYPE_TABLEAU_TEXTE)
			{
				$lignes = file($GLOBALS['cheminRacine']."config/".$fichier);
				
				if (isset($confFichier["separation"])) 
				{
					$configuration->setSeparation($confFichier["separation"]);
					$tblFinal = array();
					foreach ($lignes as $ligne)
						$tblFinal[] = $explode($confFichier["separation"], $ligne);
					$configuration->setContenu($tblFinal);
				}
				else $configuration->setContenu($lignes);
				
				return $configuration;
			}
			else if ($type == Configuration::TYPE_PHP || $type == Configuration::TYPE_TABLEAU_PHP)
			{
				require ($GLOBALS['cheminRacine']."config/".$fichier);
				$configuration->setVariables($confFichier["variables"]);
				
				$variables = array();
				foreach ($confFichier["variables"] as $variable)
					$variables[$variable] = $$variable;
				$configuration->setContenu($variables);
				
				return $configuration;
			}
			else return false;
		}
		
		/**
		 * Méthode writeConfiguration ()
		 * Permet d'écrire un fichier de configuration à partir de l'objet le représentant
		 * @param Configuration $configuration
		 * @return bool
		 */
		
		public function writeConfiguration (Configuration $configuration)
		{
			if ($configuration->estValide())
			{
				$debutFichierPhp = "<?php  if ( !defined('Framework') ) exit; ";
				if (!key_exists($configuration->getNomFichier(), $this->_listeFichiers->getContenu()['listeFichiers']))
				{
					$listeFichiers = $this->_listeFichiers->getContenu();
					$listeFichiers[$configuration->getNomFichier()] = array("type" => $configuration->getType());
					
					if ($configuration->getType() == Configuration::TYPE_TABLEAU_TEXTE && !empty($configuration->getSeparation())) 
						$listeFichiers[$configuration->getNomFichier()]["separation"] = $configuration->getSeparation();
					else if ($configuration->getType() == Configuration::TYPE_PHP || $configuration->getType() == Configuration::TYPE_TABLEAU_PHP)
						$listeFichiers[$configuration->getNomFichier()]["variables"] = $configuration->getVariables();
					
					file_put_contents($GLOBALS['cheminRacine'].'config/listeFichiers.php', $debutFichierPhp.' $listeFichiers = '.var_export($listeFichiers, true).' ?>');
					$this->setListeFichiers();
				}
				if ($configuration->getType() == Configuration::TYPE_TEXTE)
				{
					file_put_contents($GLOBALS['cheminRacine'].'config/'.$configuration->getNomFichier(), $configuration->getContenu());
				}
				else if ($configuration->getType() == Configuration::TYPE_TABLEAU_TEXTE)
				{
					$contenu = "";
					foreach ($configuration->getContenu() as $ligne)
					{
						if (!empty($configuration->getSeparation()))
							$contenu .= implode ($configuration->getSeparation(), $ligne);
						else $contenu .= $ligne;
						$contenu .= "
";
					}
					file_put_contents($GLOBALS['cheminRacine'].'config/'.$configuration->getNomFichier(), $contenu);
				}
				else if ($configuration->getType() == Configuration::TYPE_TABLEAU_PHP || $configuration->getType() == Configuration::TYPE_PHP)
				{
					$contenu = $debutFichierPhp;
					foreach($configuration->getVariables() as $variable)
						$contenu .= ' $'.$variable.' = '.var_export($configuration->getContenu()[$variable], true)."; 
";
					file_put_contents($GLOBALS['cheminRacine'].'config/'.$configuration->getNomFichier(), $contenu.' ?>');
				}
				else return false;
			}
			else return false;
		}
	}
	
?>