<?php
#################################################
#						#
#	CacheManager.php			#
#	class pour gérer la class		#
#	Cache					#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;
	
	class CacheManager
	{
		protected $_repertoire;
		protected $_chemin;
		protected $_expire;
		protected $_memcached = false;
		
		public function __construct ()
		{
			//if (class_exists('Memcached')) $this->setMemcached();
			$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
		}
		
		public function setMemcached ()
		{
			if (class_exists('Memcached')) 
			{
				$this->_memcached = new Memcached;
				$this->_memcached->addServer('localhost', 11211) or exit ('Server Error: memcached down.');
			}
			else return false;
		}
		
		public function readCache (Cache $objetCache)
		{
			$this->setExpire(time()-$objetCache->getDuree());
			$this->setChemin($objetCache->getNom());
			
			if ($this->_memcached !== false) // On vérifie qu'on est connecté au serveur memcached
			{
				$get = $this->_memcached->get($this->_chemin);
				if ($get !== false) 
				{
					$objetCache->setContenu($get);
					return true; 
				}
				else if ($this->_memcached->getResultCode() == Memcached::RES_NOTFOUND) return false;
			}
			if(file_exists($this->_chemin) && (filemtime($this->_chemin) > $this->_expire || $objetCache->getDuree() == 0) ) // 0 = pas de raffraichissement du cache
			{
				$objetCache->setContenu(unserialize(file_get_contents($this->_chemin)));
				return true;
			}
			else return false;
		}
		
		public function writeCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom());
			$this->setExpire(time()+$objetCache->getDuree());
			
			if ($this->_memcached !== false)
			{
				if ($objetCache->getDuree() != 0) $enregistre = $this->_memcached->set($this->_chemin, $objetCache->getContenu(), $this->_expire);
				else $enregistre = $this->_memcached->set($this->_chemin, $objetCache->getContenu()); 
				
				if ($enregistre) return true;
			}
			file_put_contents($this->_chemin, serialize($objetCache->getContenu()));
		}
		
		public function videCache (Cache $objetCache)
		{
			$this->setChemin($objetCache->getNom());
			if ($this->_memcached !== false)
			{
				$this->_memcached->delete($this->_chemin);
			}
			if (file_exists($this->_chemin)) ($this->_chemin);
		}
		
		public function setRepertoire ($repertoire)
		{
			if (!is_string($repertoire)) 
			{
				trigger_error('Le chemin du répertoire doit être une chaîne de caractères.', E_USER_WARNING);
				return;
			}
			$this->_repertoire = $repertoire;
		}
		
		public function setChemin ($fichier)
		{
			if (!is_string($fichier) || !isset($this->_repertoire)) 
			{
				trigger_error('Le fichier doit être une chaine de caractères et le répertoire doit être défini.', E_USER_WARNING);
				return;
			}
			$this->_chemin = $this->_repertoire.$fichier;
		}
		
		public function setExpire ($expire)
		{
			if (!is_int($expire)) 
			{
				trigger_error('$expire doit être un entier indiquant le temps en secondes.', E_USER_WARNING);
				return;
			}
			$this->_expire = $expire;
		}
		
		public function viderAllCache ()
		{
			$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
			$handle=opendir($this->_repertoire);
			while (false !== ($fichier = readdir($handle))) 
				if (($fichier != ".") && ($fichier != "..")) 
					unlink($this->_repertoire.$fichier);
			
			if ($this->_memcached !== false)
			{
				foreach ($this->_memcached->getAllKeys() as $value)
					$this->_memcached->delete($value);
			}
		}
		
		public function videCategorieCache ($nom)
		{
			if (!empty($nom))
			{
				$this->setRepertoire($GLOBALS['cheminRacine'].'cache/');
				$handle=opendir($this->_repertoire);
				while (false !== ($fichier = readdir($handle))) 
					if (($fichier != ".") && ($fichier != "..") && preg_match("/".$nom."/i", $fichier)) 
						unlink($this->_repertoire.$fichier);
			
				if ($this->_memcached !== false)
				{
					foreach ($this->_memcached->getAllKeys() as $value)
						if (preg_match("/".$nom."/i", $value)) 
							$this->_memcached->delete($value);
				}
			}
		}
	}
	
?>