<?php
#################################################
#						#
#	Cache.php				#
#	class pour mettre des données		#
#	en cache				#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;
	
	class Cache 
	{
		private $_nom;
		private $_duree;
		protected $_contenu;
		
		public function __construct ($nom, $duree)
		{
			$this->setNom($nom);
			$this->setDuree($duree);
		}
		
		public function getContenu ()
		{
			return $this->_contenu;
		}
		
		public function getDuree ()
		{
			return $this->_duree;
		}
		
		public function getNom ()
		{
			return $this->_nom;
		}
		
		public function setContenu ($contenu)
		{
			$this->_contenu = $contenu;
		}
		
		public function setNom ($nom)
		{
			if (!is_string($nom)) 
			{
				trigger_error('Le nom doit être une chaîne de caractères.', E_USER_WARNING);
				return;
			}
			$this->_nom = $nom;
		}
		
		public function setDuree ($duree)
		{
			if (!is_int($duree)) 
			{
				trigger_error('La durée doit être un nombre entier indiquant le nombre de secondes.', E_USER_WARNING);
				return;
			}
			$this->_duree = $duree;
		}
	}
	
?>