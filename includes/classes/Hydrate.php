<?php
#################################################
#						#
#	Hydrate.php				#
#	trait pour la méthode d'hydratation	#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;   
	
	trait Hydrate
	{
		/**
		 * Méthode hydrate()
		 * Hydrate les attributs de l'objet
		 * @param array $donnees
		 * @return void
		 */
		  
		public function hydrate (array $donnees)
		{
			foreach ($donnees as $key => $value)
			{
				$methode = 'set'.ucfirst($key);
				if (method_exists($this, $methode)) $this->$methode($value);
			}
		}
	}
	
?>