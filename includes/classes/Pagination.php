<?php
#################################################
#						#
#	Pagination.php				#
#	class pour paginer du contenu		#
#	Créateur : Guillaume KIHLI		#
#						#
################################################# 

	if ( !defined('Framework') ) exit;  
 
	class Pagination
	{
		private $_elementsParPage;
		private $_nombreTotalElements;
		private $_nombrePages;
		private $_pageActuelle 		= 1;
		private $_transmission 		= '';
		private $_premier 		= false; // booléen pour indiquer si ce que l'on va ajouter à la fin du lien est en premier ou non (pour savoir si utiliser & ou ?)
		private $_premiereEntree 	= 0;
		private $_url;
		
		const DERNIERE_PAGE = 0;
		const SEPARATION = '...';
		
		public function __construct ($elementsParPage, $nombreTotalElements, $url)
		{
			$this->setElementsParPage($elementsParPage);
			$this->setNombreTotalElements($nombreTotalElements);
			$this->setNombrePages();
			$this->setUrl($url);
		}
		
		public function setElementsParPage ($elementsParPage)
		{
			if (is_numeric($elementsParPage)) $this->_elementsParPage = $elementsParPage;
			else trigger_error('Le nombre d\'éléments par page doit être un entier !.', E_USER_WARNING);
		}
		
		public function setNombreTotalElements ($nombreTotalElements)
		{
			$this->_nombreTotalElements = intval($nombreTotalElements);
		}
		
		public function setPageActuelle ($pageActuelle)
		{
			if (is_numeric($pageActuelle) || $pageActuelle == SELF::DERNIERE_PAGE) 
			{
				if ($pageActuelle == SELF::DERNIERE_PAGE)
					$this->_pageActuelle = $this->_nombrePages;
				else
					$this->_pageActuelle = intval($pageActuelle);
				$this->setPremiereEntree();
			}
			else trigger_error('La page actuelle doit être un entier !.', E_USER_WARNING);
		}
		
		public function addTransmission (Array $transmission)
		{
			foreach ($transmission as $clef => $valeur)
			{
				 $this->_transmission .= "&amp;".$clef."=".$valeur;
			}
		}
		
		public function setPremier ($premier)
		{
			if (is_bool($premier)) $this->_premier = $premier;
			else trigger_error('La page actuelle doit être un entier !.', E_USER_WARNING);
		}
		
		public function setNombrePages ()
		{
			$this->_nombrePages = ceil($this->_nombreTotalElements/$this->_elementsParPage);
		}
		
		public function setPremiereEntree ()
		{
			$this->_premiereEntree = ($this->_pageActuelle-1)*$this->_elementsParPage;
			if ($this->_premiereEntree < 0) $this->_premiereEntree = 0;
		}
		
		public function setUrl ($url)
		{
			if (is_string($url)) $this->_url = $url;
			else return false;
		}
		
		public function getLimit () // fonction retournant le LIMIT pour la requête SQL
		{
			if ($this->_pageActuelle > $this->_nombrePages) $this->setPageActuelle($this->_nombrePages);
			$this->setPremiereEntree();
			return " LIMIT ".$this->_elementsParPage." OFFSET ".$this->_premiereEntree;
		}
		
		public function formeBaliseFinale ($page, $contenu = null)
		{
			if ($contenu != SELF::SEPARATION)
			{
				$retour = '';
				
				if ($page == $this->_pageActuelle) $retour .= '<a class="pageActuelle" href="'.$this->_url;
				else $retour .= '<a class="page" href="'.$this->_url;
				
				if ($this->_premier)  $retour .= '?page=';
				else $retour .= '&amp;page=';
				
				if ($contenu == null) $contenu = $page;
				$retour.= $page.$this->_transmission.'">'.$contenu.'</a>';
			}
			else $retour = '<a class="page" href="#">'.SELF::SEPARATION.'</a>';
			
			return $retour;
		}
		
		public function getPages ()
		{
			$retour = array();
			
			// Bouton page précédente
			if ($this->_pageActuelle > 1) $retour[] = $this->formeBaliseFinale($this->_pageActuelle-1, '&lt;');
			
			if ($this->_nombrePages < 10) // Moins de 10 pages à afficher : on affiche toutes les pages disponibles
			{
				for($i = 1; $i <= $this->_nombrePages; $i++)
					$retour[] = $this->formeBaliseFinale($i); 
			}
			else
			{
				if ($this->_pageActuelle <= 5) // La page actuelle est vers le début de la liste de pages
				{
					for($i = 1; $i <= $this->_pageActuelle+2; $i++) // De la page 1 jusqu'à deux pages après la page actuelle
						$retour[] = $this->formeBaliseFinale($i); 
					
					$retour[] = $this->formeBaliseFinale(0, SELF::SEPARATION); // Séparation (pour symboliser le saut de pages)
					
					for($i = $this->_nombrePages-2; $i <= $this->_nombrePages; $i++) // On affiche les 3 dernières pages
						$retour[] = $this->formeBaliseFinale($i); 
				}
				elseif ($this->_pageActuelle < $this->_nombrePages-5) // La page actuelle est vers le milieu de la liste
				{
					for($i = 1; $i <= 2; $i++) // De la page 1 à la 2
						$retour[] = $this->formeBaliseFinale($i); 
					
					$retour[] = $this->formeBaliseFinale(0, SELF::SEPARATION);
					
					for($i = $this->_pageActuelle-2; $i <= $this->_pageActuelle+2; $i++) // On affiche deux pages autour de la page actuelle
						$retour[] = $this->formeBaliseFinale($i); 
					
					$retour[] = $this->formeBaliseFinale(0, SELF::SEPARATION);
					
					for($i = $this->_nombrePages-1; $i <= $this->_nombrePages; $i++) // On affiche les deux dernières pages
						$retour[] = $this->formeBaliseFinale($i); 
				}
				else // La page actuelle est vers la fin de la liste
				{
					for($i = 1; $i <= 2; $i++) // On affiche les deux premières pages
						$retour[] = $this->formeBaliseFinale($i); 
						
					$retour[] = $this->formeBaliseFinale(0, SELF::SEPARATION);
					
					for($i = $this->_pageActuelle-2; $i <= $this->_nombrePages; $i++) // On affiche à partir de 2 pages en arrière de la page actuelle, jusqu'à la fin
						$retour[] = $this->formeBaliseFinale($i); 
				}
			}
			
			// Bouton page suivante
			if ($this->_pageActuelle != $this->_nombrePages) $retour[] = $this->formeBaliseFinale($this->_pageActuelle+1, '&gt;');
			
			return $retour;
		}
		
		public function getNombreTotalElements () { return $this->_nombreTotalElements; }
		public function getPremiereEntree() { return $this->_premiereEntree; }
		public function getElementsParPage () { return $this->_elementsParPage; }
		public function getPageActuelle () { return $this->_pageActuelle; }
	}