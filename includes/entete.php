<?php

	if ( !defined('Framework') ) exit;
	
	$tplEntete = new Smarty;
	
	// On assigne afin de pouvoir afficher dans le template
	$tplEntete->assign(array(
		'titrePage' => $titre,
		'cheminRacine' => $cheminRacine
		));
		
	$tplEntete->display('entete.html');

?> 
