<?php

define('Framework', true);
$titre = 'Séquences';
$cache = false;
require_once('includes/init.php');
$tpl = new Smarty;
require_once('includes/entete.php');

$semanticalBioDeviceManager = new SemanticalBioDeviceManager($bdd);
$booleanFunctionManager = new BooleanFunctionManager($bdd);
$permutationClassManager = new PermutationClassManager($bdd);

try 
{
	if (isset($_GET['permutation_class']))
	{
		$permutationClass = $permutationClassManager->getPermutationClass($_GET['permutation_class']);
		
		$tpl->assign(array(
			'permutationClass' => $permutationClass));
			
		$tpl->display('permutation_class.html');
	}
	if (isset($_GET['boolean_function']))
	{
		$booleanFunction = new BooleanFunction();
		$booleanFunction->hydrate(["dnf" => $_GET['boolean_function']]);
		$veritas = new VeritasBooleanFunction($booleanFunction);
		
		$tpl->assign(array(
			'booleanFunction' => $booleanFunction,
			'veritas' => $veritas,
			'notBottom' => true));
			
		$tpl->display('fonction.html');
	}
}
catch (Exception $e)
{
	$tpl->assign(array(
		'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
}

$tpl = new Smarty;

$parameters = array('permutation_class', $_GET['permutation_class']);

if (isset($_GET['semanticalBioDevice']))
{
	$parameters = array_merge($parameters, array(DB::SQL_AND, 'semanticalBioDevice', $_GET['semanticalBioDevice']));
}

if (isset($_GET['permutation_class']))
{
	$pageURI = 'listSeq.php?permutation_class='.$_GET['permutation_class'];
	
	if (isset($_GET['semanticalBioDevice']))
	{
		$pageURI .= '&semanticalBioDevice='.$_GET['semanticalBioDevice'];
	}
	$pagination = new Pagination(
		30, 
		$semanticalBioDeviceManager->getNombre($_GET['permutation_class']), $pageURI);
	
	if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
	$pagination->setPremier(false);
}
else
{
	$pagination = new Pagination(30, $semanticalBioDeviceManager->getNombre(), 'listSeq.php');
	if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
		$pagination->setPremier(true);
}

if (isset($_GET['permutation_class']))
	$liste =  $semanticalBioDeviceManager->getListe($pagination, $parameters, array(['champ' => 'weak_constraint', 'sens' => DB::ORDRE_DESC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
else
	$liste =  $semanticalBioDeviceManager->getListe($pagination, null, array(['champ' => 'weak_constraint', 'sens' => DB::ORDRE_DESC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));

$tpl->assign(array(
	'listeSequences' => $liste,
	'pages' => $pagination->getPages()));
	
	$tpl->display('listSeq.html');
	require_once('includes/piedDePage.php');
	