<?php

	define('Framework', true);
	$cache = false;
	require_once('includes/init.php');
	$titre = t('Chercher des séquences génétiques');
	$tpl = new Smarty;
	
	if (isset($_GET['fonction'])) 
	{
	    try 
	    {
		if (empty($_GET['fonction']))
		    throw new exception(t('La fonction ne peut être vide'));
		
		$fonction = str_replace('-', '+', urldecode($_GET['fonction']));
		
		$booleanFunction = new BooleanFunction($fonction);
		$veritas = new VeritasBooleanFunction($booleanFunction);
		
		setcookie ("fonction", $fonction, time() + 365*24*3600);
		
		$tpl->assign(array(
			'booleanFunction' => $booleanFunction,
			'veritas' => $veritas));
			
		$tpl->display('fonction.html');
	    }
	    catch (Exception $e)
	    {
		$tpl->assign(array(
			'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
	    }
	}
	else if (isset($_GET['fonctionSearch'])) 
	{
	    try 
	    {
		if (empty($_GET['fonctionSearch']))
		    throw new exception(t('La séquence ne peut être vide'));
		
		$fonction = str_replace('-', '+', urldecode($_GET['fonctionSearch']));
		
		$booleanFunction = new BooleanFunction($fonction);
		$veritas = new VeritasBooleanFunction($booleanFunction);
		
		setcookie ("fonction", $fonction, time() + 365*24*3600);
		
		$semanticalBioDeviceManager = new SemanticalBioDeviceManager($bdd);
		$booleanFunctionManager = new BooleanFunctionManager($bdd);
 		
		$pagination = new Pagination(30, $semanticalBioDeviceManager->getNombre($booleanFunctionManager->getBooleanFunction(
		    ['ndf',bindec($veritas->getMinimalOutput ()),DB::SQL_AND,'nb_inputs',$veritas->getMinimalNbInputs()])->getId_fn()), 
		    'listSeq.php?output='.$veritas->getMinimalOutput ()."&amp;nbInputs=".$veritas->getMinimalNbInputs());
		if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
		$pagination->setPremier(false);
		
		$liste =  $semanticalBioDeviceManager->getListe($pagination, 
		    ['ndf',bindec($veritas->getMinimalOutput ()),DB::SQL_AND,'nb_inputs',$veritas->getMinimalNbInputs()], 
		    array(['champ' => 'weak_constraint', 'sens' => DB::ORDRE_DESC], ['champ' => 'length', 'sens' => DB::ORDRE_ASC]));
		    
		$tpl->assign(array(
			'listeSequences' => $liste,
			'pages' => $pagination->getPages()));
		
		$tpl->display('listSeq.html');
	    }
	    catch (Exception $e)
	    {
		$tpl->assign(array(
			'erreur' => $e->getMessage()));
		
		$tpl->display('erreurAjax.html');
	    }
	}
	else 
	{
	    if (isset($_COOKIE['fonction']))
		$tpl->assign(array(
			'fonction' => $_COOKIE['fonction']));
	    else
		$tpl->assign(array(
			'fonction' => ''));
		    
	    require_once('includes/entete.php'); 
	    $tpl->display('searchSeq.html');
	    require_once('includes/piedDePage.php');
	} 
