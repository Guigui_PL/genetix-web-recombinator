<?php

define('Framework', true);
$cache = false;
require_once('includes/init.php');
$titre = t('Liste des fonctions');
$tpl = new Smarty;

require_once('includes/entete.php');

$permutationClassManager = new PermutationClassManager($bdd);

$pagination = new Pagination(30, $permutationClassManager->getNombre(), 'index.php');
if (isset($_GET['page'])) $pagination->setPageActuelle($_GET['page']);
$pagination->setPremier(true);

if (!empty($_GET['ordre']))
	$pagination->addTransmission(['ordre' => $_GET['ordre']]);

if (!empty($_GET['sens']))
	$pagination->addTransmission(['sens' => $_GET['sens']]);

if (!empty($_GET['ordre']))
{
	switch ($_GET['ordre'])
	{
		default:
			$orderBy = array(['champ' => 'nb_inputs', 'sens' => DB::ORDRE_ASC], ['champ' => 'permutation_class', 'sens' => DB::ORDRE_ASC]);
			
	}
}
else 
	$orderBy = array(['champ' => 'nb_inputs', 'sens' => DB::ORDRE_ASC], ['champ' => 'permutation_class', 'sens' => DB::ORDRE_ASC]);

$tpl->assign(array(
	'listePermutationClass' => $permutationClassManager->getListe($pagination, null, $orderBy),
	'pages' => $pagination->getPages()));
	
	$tpl->display('index.html');
	require_once('includes/piedDePage.php');
	